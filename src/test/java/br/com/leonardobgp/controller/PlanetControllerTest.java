package br.com.leonardobgp.controller;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import br.com.leonardobgp.error.NotAcceptableException;
import br.com.leonardobgp.model.Planet;
import br.com.leonardobgp.service.PlanetService;

@RunWith(SpringRunner.class)
@WebMvcTest(value = PlanetController.class)
public class PlanetControllerTest {
	
	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private PlanetService planetService;
	


	Planet mockPlanet1 = new Planet("Alderaan", "temperate","grasslands, mountains",2);
	Planet mockPlanet2 = new Planet(" Alderaan", "temperate","grasslands, mountains",2);
	Planet mockPlanet3 = new Planet("alderaan", "temperate","grasslands, mountains",2);
	Planet mockPlanet4 = new Planet("Yavin IV", "temperate, tropica", "jungle, rainforests", 1);
	
	String jsonPlanetModel = "{"
			+"\"id\":null,"
			+ "\"name\":\"ALDERAAN\","
			+ "\"climate\":\"TEMPERATE\","
			+ "\"terrain\":\"GRASSLANDS, MOUNTAINS\","
			+ "\"countApparitions\":2}";
	
	String jsonPlanetModelCreate = "{"
			+ "\"name\":\"ALDERAAN\","
			+ "\"climate\":\"TEMPERATE\","
			+ "\"terrain\":\"GRASSLANDS, MOUNTAINS\"}";
	
	String jsonPlanetModelCreate2 = "{"
			+ "\"name\":\"Yavin IV\","
			+ "\"climate\":\"temperate, tropica\","
			+ "\"terrain\":\"jungle, rainforests\"}";
	
	String txtCreate = "'name=Alderaan&climate=temperate&terrain=grasslands, mountains'";
	
	@Test
	public void readOnePlanetByName() throws Exception {

		Mockito.when(planetService.getByName(Mockito.anyString())).thenReturn(mockPlanet1);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/planets/name/?name=Alderaan").accept(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		System.out.println(result.getResponse());
		String expected = jsonPlanetModel;
		JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);;
	}

	@Test
	public void readOnePlanetByNameWithBlankSpace() throws Exception {

		Mockito.when(planetService.getByName(Mockito.anyString())).thenReturn(mockPlanet2);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/planets/name/?name=Alderaan").accept(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		System.out.println(result.getResponse());
		String expected = jsonPlanetModel;
		JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);;
	}
	
	@Test
	public void readOnePlanetByNameWithDifferentCase() throws Exception {

		Mockito.when(planetService.getByName(Mockito.anyString())).thenReturn(mockPlanet3);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/planets/name/?name=Alderaan").accept(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		System.out.println(result.getResponse());
		String expected = jsonPlanetModel;
		JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);;
	}
	
	@Test
	public void readOnePlanetDoesntExist() throws Exception {

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/planets/name/?name=Sayajin").accept(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		System.out.println(result.getResponse());
		assertEquals(404, result.getResponse().getStatus());
	}
	
	@Test
	public void create() throws Exception{
		
		Mockito.when(planetService.createPlanet(Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn(mockPlanet1);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.post("/planets")
				.accept(MediaType.APPLICATION_JSON).content(jsonPlanetModelCreate)
				.contentType(MediaType.APPLICATION_JSON);

				
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		
		assertEquals(HttpStatus.CREATED.value(), result.getResponse().getStatus());
	}
	
	@Test
	public void update() throws Exception{
		
		Planet planet = new Planet("Yavin IV", "temperate, tropica", "jungle, rainforests", 1);
		planet.setId("1231abc456def");
		
		Mockito.when(planetService.updatePlanetById(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn(mockPlanet1);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.put("/planets/1231abc456def")
				.accept(MediaType.APPLICATION_JSON).content(jsonPlanetModelCreate)
				.contentType(MediaType.APPLICATION_JSON);
								
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		
		assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
	}
	
	@Test
	public void createDuplicate() throws Exception{
		
		Planet planet = new Planet("Yavin IV", "temperate, tropica", "jungle, rainforests", 1);
		planet.setId("1231abc456def");
		
		Mockito.when(planetService.getByName(Mockito.anyString())).thenReturn(planet);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.post("/planets")
				.accept(MediaType.APPLICATION_JSON).content(jsonPlanetModelCreate2)
				.contentType(MediaType.APPLICATION_JSON);

				
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		
		assertEquals(HttpStatus.NOT_ACCEPTABLE.value(), result.getResponse().getStatus());
	}
	
	@Test
	public void updateDuplicate() throws Exception{
		
		Planet planet = new Planet("Yavin IV", "temperate, tropica", "jungle, rainforests", 1);
		planet.setId("1231abc456def");
		
		Mockito.when(planetService.updatePlanetById(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenThrow(NotAcceptableException.class);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.put("/planets/1231abc456def")
				.accept(MediaType.APPLICATION_JSON).content(jsonPlanetModelCreate2)
				.contentType(MediaType.APPLICATION_JSON);

				
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		
		assertEquals(HttpStatus.NOT_ACCEPTABLE.value(), result.getResponse().getStatus());
	}
	
}
