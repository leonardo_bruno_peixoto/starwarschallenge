package br.com.leonardobgp.response;

import java.util.ArrayList;

import br.com.leonardobgp.model.PlanetAPI;

public class PlanetResponse {

	ArrayList<PlanetAPI> results;

	public ArrayList<PlanetAPI> getResults() {
		return results;
	}

	public void setResults(ArrayList<PlanetAPI> results) {
		this.results = results;
	}

}
