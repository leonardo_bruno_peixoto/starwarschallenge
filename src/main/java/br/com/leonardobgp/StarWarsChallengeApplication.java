package br.com.leonardobgp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StarWarsChallengeApplication {

	public static void main(String[] args) {
		SpringApplication.run(StarWarsChallengeApplication.class, args);
	}
}
