package br.com.leonardobgp.controller;

import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.leonardobgp.error.NotAcceptableException;
import br.com.leonardobgp.error.NotFoundException;
import br.com.leonardobgp.model.Planet;
import br.com.leonardobgp.service.PlanetService;

@RestController
public class PlanetController {

	@Autowired
	private PlanetService planetService;
	
	
	@RequestMapping(value = "/planets", method = RequestMethod.POST)
	@ResponseBody
	@ResponseStatus(HttpStatus.CREATED)
	public Planet create(@RequestBody Planet planet) throws NotAcceptableException  {
		Planet planetResult = planetService.getByName(planet.getName());
		if(planetResult == null) {
			planetResult = planetService.createPlanet(planet.getName(), planet.getClimate(), planet.getTerrain());
			return planetResult;
		} else {
			throw new NotAcceptableException("This planet already exists!");
		}
	}
	
	@RequestMapping("/planets/name/")
	public Planet readByName(@RequestParam String name) throws NotFoundException {
		Planet planet = planetService.getByName(name);
		if(planet == null) {
			throw new NotFoundException("Planet not found!"); 
		} else{
			return planet;
		}		
	}
	
	@RequestMapping(value = "/planets/{id}", method = RequestMethod.GET)
	public Planet readByID(@PathVariable("id") String id) throws NotFoundException {
		Planet planet = planetService.getById(id);
		if(planet == null) {
			throw new NotFoundException("Planet not found!"); 
		} else{
			return planet;
		}
	}
	
	@RequestMapping(value = "/planets", method = RequestMethod.GET)
	public List <Planet> readAll(){
		List <Planet> planets = planetService.getAll();
		return planets;
	}
	
	@RequestMapping(value = "/planets/{id}", method = RequestMethod.PUT)
	@ResponseBody
	public Planet updateById(@PathVariable("id") String id, @RequestBody Planet planet) throws NotFoundException, NotAcceptableException {
		Planet planetResult = planetService.updatePlanetById(id, planet.getName(), planet.getClimate()
				, planet.getTerrain());
		return planetResult;
	}
	
//	@RequestMapping("/planets/{name}")
//	@ResponseBody
//	public Planet updateByName(@RequestBody Planet planet) throws NotFoundException {
//		Planet planetResult = planetService.updatePlanetByName(planet.getName(), planet.getClimate(), planet.getTerrain());
//		return planetResult;
//	}
	
	@RequestMapping(value = "/planets/{name}", method = RequestMethod.DELETE)
	public String deleteOne(@PathVariable("name") String name) {
		planetService.deathstarRay(name);
		return name+" was destroyed as wished Lord Vader";
	}
	
	@RequestMapping(value = "/planets/", method = RequestMethod.DELETE)
	public String deleteAll() {
		planetService.destroyTheGalaxy();;
		return "All glory to Empire";
	}
}
