package br.com.leonardobgp.service;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import br.com.leonardobgp.error.NotAcceptableException;
import br.com.leonardobgp.error.NotFoundException;
import br.com.leonardobgp.model.Planet;
import br.com.leonardobgp.model.PlanetAPI;
import br.com.leonardobgp.repository.PlanetRepository;
import br.com.leonardobgp.response.PlanetResponse;

@Service
public class PlanetService {

	@Autowired
	public PlanetRepository planetRepository;

	@Autowired
	private RestTemplate restTemplate;

	ArrayList <String> films = new ArrayList <String>();

	//CRUD Operations
	//Create
	public Planet createPlanet(String name, String climate, String terrain) {
		Integer count = this.countApparitions(name.trim());
		return planetRepository.save(new Planet(name,climate,terrain,count));
	}

	//Read all
	public List<Planet> getAll(){
		return planetRepository.findAll();
	}

	//Read one by ID
	public Planet getById(String id) {
		Optional<Planet> planetReturn = planetRepository.findById(id);
		if (planetReturn.isPresent()) {
			Planet planet = planetReturn.get();
			return planet;
		} else {
			return null;
		}

	}

	//Read one by name
	public Planet getByName(String name) {
		return planetRepository.findByName(name.trim().toUpperCase());
	}

	//Update by ID
	public Planet updatePlanetById (String id, String name, String climate, String terrain) throws NotFoundException, NotAcceptableException {
		Optional<Planet> planetReturn = planetRepository.findById(id);
		if (planetReturn.isPresent()) {
			Planet planet = planetReturn.get();
			if(getByName(planet.getName()) == null) {
				planet.setName(name.trim().toUpperCase());
				planet.setClimate(climate.trim().toUpperCase());
				planet.setTerrain(terrain.trim().toUpperCase());
				return planetRepository.save(planet);
			} else {
				throw new NotAcceptableException("This planet name already exists!");
			}
			
		}
		else {
			throw new NotFoundException("Planet not found!");
		}
	}

	//Update by Name
	public Planet updatePlanetByName (String name, String climate, String terrain) throws NotFoundException {
		Planet planet = planetRepository.findByName(name.trim().toUpperCase());
		if(planet == null) {
			planet.setName(name);
			planet.setClimate(climate);
			planet.setTerrain(terrain);
			return planetRepository.save(planet);
		} else {
			throw new NotFoundException("Planet not found!");
		}
	}


	//Delete All
	public void destroyTheGalaxy() {
		planetRepository.deleteAll();
	}

	//Delete One
	public void deathstarRay(String name) {
		Planet planet = planetRepository.findByName(name.trim().toUpperCase());
		planetRepository.delete(planet);
	}

	public Integer countApparitions(String name) {

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

		ResponseEntity <PlanetResponse> planetResponse = restTemplate.exchange(
				URI.create("https://swapi.co/api/planets/?search="+name),
				HttpMethod.GET,
				entity,
				PlanetResponse.class);
		ArrayList<PlanetAPI> planetAPIs = planetResponse.getBody().getResults();
		if(planetAPIs.isEmpty()) {
			return 0;
		}else {
			return planetAPIs.get(0).getFilms().size();	
		}
	}
}
