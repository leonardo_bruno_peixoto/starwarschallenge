package br.com.leonardobgp.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import br.com.leonardobgp.model.Planet;

@Repository
public interface PlanetRepository extends MongoRepository<Planet, String> {
	
	public Planet findByName(String name);
	
	public Optional<Planet> findById(String Id);

}
