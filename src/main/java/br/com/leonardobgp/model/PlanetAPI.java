package br.com.leonardobgp.model;

import java.util.ArrayList;

public class PlanetAPI {

	ArrayList <String> films;

	public ArrayList<String> getFilms() {
		return films;
	}

	public void setFilms(ArrayList<String> films) {
		this.films = films;
	}
}
