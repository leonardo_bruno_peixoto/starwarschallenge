package br.com.leonardobgp.model;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Planet {

	//Attributes
	@Id
	String id;
	String name;
	String climate;
	String terrain;
	Integer countApparitions;
	
	//Getters
	public String getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public String getClimate() {
		return climate;
	}
	public String getTerrain() {
		return terrain;
	}
	public Integer getCountApparitions() {
		return countApparitions;
	}
	
	//Setters
	public void setId(String id) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setClimate(String climate) {
		this.climate = climate;
	}
	public void setTerrain(String terrain) {
		this.terrain = terrain;
	}
	public void setCountApparitions(Integer countApparitions) {
		this.countApparitions = countApparitions;
	}
	//Constructor
	public Planet(String name, String climate, String terrain, Integer countApparitions) {
		this.name = name.trim().toUpperCase();
		this.climate = climate.trim().toUpperCase();
		this.terrain = terrain.trim().toUpperCase();
		this.countApparitions = countApparitions; 
	}
	
	public Planet() {
	}
}
